" My current NeoVim init.vim file
" Jack Baty
" jack@baty.net
" Twitter: @jackbaty

call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes

Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'vimwiki/vimwiki'
Plug 'kien/ctrlp.vim'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'reedes/vim-wordy'
Plug 'junegunn/goyo.vim'
Plug 'reedes/vim-pencil'

" Initialize plugin system
call plug#end()


" Settings ---------------------------------------------------------------
set ruler " show the ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " a ruler on steroids
set showcmd                 " show partial commands in status line and
            			    " selected characters/lines in visual mode
set scrolloff=3             " keep at least 3 lines visible around cursor
set showmode                " so we know which mode we're in
if has('statusline')
    set laststatus=2
    " Broken down into easily-includeable segments
    set statusline=%<%f\                     " Filename
    set statusline+=%w%h%m%r                 " Options
    set statusline+=%{fugitive#statusline()} "  Git Hotness
    set statusline+=\ [%{&ff}/%Y]            " filetype
    set statusline+=\ [%{getcwd()}]          " current dir
    set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
endif
set ignorecase              " case-insensitive search...
set smartcase               " ...unless there's some uppercase present
set gdefault                " default to /g flag during substitutions
set showmatch               " indicate matching brackets and parens
set scrolljump=5            " lines to scroll when cursor leaves screen
set ts=4                    " Tabs are 4 spaces
set textwidth=79            " wrap width
set wrap                    " wrap on (since I rarely write code anyway)
set formatoptions=t         "
set linebreak               " wrap at boundaries
set wm=2                    " leave a little margin when wrapping
set softtabstop=4           " Backspace over everything in insert mo
set shiftwidth=4            " Tabs under smart indent
set expandtab
set foldlevelstart=20       " not a fan of automatic folding<Paste>
set list listchars=tab:\ \ ,trail:·  " Display tabs and trailing spaces visually


" Mappings and Shortcuts -------------------------------------------------

let mapleader=","
nnoremap Q gqip             " hard wrap

" Yank text to the OS X clipboard
noremap <leader>y "*y
noremap <leader>yy "*Y

map <C-n> :NERDTreeToggle<CR>

" Spelling
set spell spelllang=en_us
nnoremap <leader>f 1z=    " use the first recommended replacement
nnoremap <leader>s :setlocal spell! spelllang=en_us<CR> " toggle spellcheck

" easier window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


" Create PDF from markdown/pandoc files
nnoremap ,pdf  :!/usr/local/bin/pandoc --latex-engine=xelatex --template=/Users/jbaty/.pandoc/templates/plain-xelatex.template "%" -o "%".pdf
nnoremap ,doc  :!pandoc "%" -o "%".docx
nnoremap ,fpdf  :!/usr/local/bin/pandoc --latex-engine=xelatex --template=/Users/jbaty/.pandoc/templates/fusionary.template "%" -o "%".pdf

" Open current file in BBEdit
nnoremap ,bb  :!/usr/local/bin/bbedit  "%"

"clear highlighted search
nmap <silent> <leader>/ :nohlsearch<CR>

" Nice prose editing
map <F11> :Goyo <bar> :TogglePencil <CR>

" Misc ------------------------------------------------------------------------

let g:vim_markdown_frontmatter = 1

" make table both pandoc and github lovable
let g:table_mode_separator = '|'
let g:table_mode_corner = '+'
let g:table_mode_corner_corner = '+'
let g:table_mode_header_fillchar = "="

" Pandoc
let g:pandoc#filetypes#handled = ["pandoc", "markdown"]
let g:pandoc#filetypes#pandoc_markdown = 0

au BufNewFile,BufRead *.txt   set filetype=markdown
au BufNewFile,BufRead *eml.txt   set filetype=mail
au BufNewFile,BufRead *.markdown set filetype=markdown
au BufNewFile,BufRead *.md   set filetype=markdown

" Softwrap the document
command! -range=% SoftWrap
            \ <line2>put _ |
            \ <line1>,<line2>g/.\+/ .;-/^$/ join |normal $x

let g:vimwiki_list = [{'path': '/Users/jbaty/Dropbox/wiki','syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_folding='expr'
let g:vimwiki_global_ext=0
